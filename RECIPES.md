## Gitlab dual recipes

Below is a list of scenarious and the steps needed to put them in place.

### Scenario 1: Replicate your copy of gitlab

This scenario involves migrating your current Gitlab installation
into an environment with Docker and gitlab-dual tools.

 0. stop the currently Gitlab installation - alert the team for a downtime
 1. generate a backup copy of your current gitlab installation
 2. move the .tar files into the new system with Docker and gitlab-dual
 3. install gitlab-dual package
 4. make sure to set the exact same Gitlab version at /etc/gitlab-dual-settings
 5. run gitlab-dual-cli run
 6. wait a few moments until you hit the first page
 7. set the new password as requested
 8. login as root, make sure the help page tells you the exact same gitlab version
 9. logout from the site

#### Scenario 2: Restore backup

 0. make sure the server is up and running with `gitlab-dual-cli status`
 1. make sure the backup file prefix is the same as with /etc/gitlab-dual-settings
    ** example, `v_backup_filename="mygitlab"` will restore the backup file: `mygitlab_gitlab_backup.tar`
 2. copy the backupfile under `v_gitlab_homedir/backups`.
 3. do the restore: gitlab-dual-cli restore
 4. visit the page to make sure you can login with a regular user from the original gitlab

#### Scenario 3: Run integrity checks

 1. create the file: $HOME/.python-gitlab.cfg with 2 hostnames: original Gitlab and copied hostnames
 2. use the same authorization token for both hosts
 3. run integrity checks for both hosts
    ** gitlab-dual-cli integrity hostname1
    ** gitlab-dual-cli integrity hostname2
 4. compare the collected data fingerprints - they should be identical
    ** diff hostname1.json hostname2.json

At this point it is time to switch your DNS records to point to the new server.
Alert the team that the Gitlab server is back online.

### Scenario 4: Upgrade your new Gitlab instance version

TODO

### Scenario 5: Duplicate your new Gitlab copy to a second server

 1. allocate a new server with same specs as the one on scenario 1
 2. On server one, establish an automated backup system using cron to run: `gitlab-dual-cli backup`
 3. On the new server, establish an automated restore system using cron to run: `gitlab-dual-cli restore`
 4. Double make sure to have a generous time window to avoid clashing a backup with a restore
 5. Run integrity checks again, as in Scenario 3
 6. At this point, the second server can be brought online, should the first server fail
    ** simply change the DNS records to point to the new server.
