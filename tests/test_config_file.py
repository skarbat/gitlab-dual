'''
test_config_file.py
'''

import runner

def find_config_value(bash, key):
    s = runner.run_gitlab_cli(bash, ['info'])
    assert s.find (key) != -1, \
        "ERROR: config key not found: < {} >".format(key)

def test_file_loaded(bash):
    find_config_value(bash, 'settings: tests/fixtures/settings.sh')

def test_container_name(bash):
    find_config_value(bash, 'container name: test_gitlab')

def test_gitlab_version(bash):
    find_config_value(bash, 'gitlab version: gitlab_version_12345')

def test_gitlab_homedir(bash):
    find_config_value(bash, 'gitlab homedir: tests')

def test_gitlab_hostname(bash):
    find_config_value(bash, 'gitlab hostname: gitlab_hostname')

def test_gitlab_backup_filename(bash):
    find_config_value(bash, 'backup filename: test')

def test_gitlab_backup_cfgfile(bash):
    find_config_value(bash, 'backup cfgfile: testcfg')

def test_network_name(bash):
    find_config_value(bash, 'network name: gitlab-net')

def test_mta_allowed_domains(bash):
    find_config_value(bash, 'mta allowed domains: mydomain1.com mydomain2.com')

def test_mta_allowed_domains(bash):
    find_config_value(bash, 'mta image: boky/postfix')
