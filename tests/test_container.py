'''
test_container.py
'''

import runner

def test_run(bash):
    s = runner.run_gitlab_cli(bash, ['run'])
    assert s.find('ERROR') == -1
    assert runner.find_container_name(s) == True
    runner.find_container_keys(s, ['docker', '--volume', '--publish'])

def test_status(bash):
    s = runner.run_gitlab_cli(bash, ['status'])
    assert runner.find_container_name(s) == True
    runner.find_container_keys(s, ['inspect', 'State']) == True

def test_restart(bash):
    s = runner.run_gitlab_cli(bash, ['restart'])
    assert runner.find_container_name(s) == True
    runner.find_container_keys(s, ['restart']) == True

def test_stop(bash):
    s = runner.run_gitlab_cli(bash, ['stop'])
    assert runner.find_container_name(s) == True
    runner.find_container_keys(s, ['stop', 'rm']) == True

def test_logs(bash):
    s = runner.run_gitlab_cli(bash, ['logs'])
    assert runner.find_container_name(s) == True
    runner.find_container_keys(s, ['logs', '-f']) == True

def test_check(bash):
    s = runner.run_gitlab_cli(bash, ['check'])
    assert runner.find_container_name(s) == True
    assert s.find('gitlab-rake gitlab:check') != -1

def test_shell(bash):
    s = runner.run_gitlab_cli(bash, ['shell'])
    assert runner.find_container_name(s) == True
    runner.find_container_keys(s, ['bash', 'exec', '-it']) == True

def test_reconfigure(bash):
    s = runner.run_gitlab_cli(bash, ['reconfigure'])
    assert runner.find_container_name(s) == True
    runner.find_container_keys(s, ['gitlab-ctl', 'reconfigure']) == True
