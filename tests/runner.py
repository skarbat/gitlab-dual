'''
runner.py

Helper test module to override environment variables specific for tests
'''

cli_script = 'bin/gitlab-dual-cli'
environ = { 'DOCKER': 'echo docker',
            'SETTINGS': 'tests/fixtures/settings.sh',
            'FUNCTIONS': 'share/gitlab-dual-functions',
            'INTEGRITY': 'share/integrity-checks' }

def run_gitlab_cli(bash, params):
    with bash(envvars=environ) as s:
        return s.run_script(cli_script, params)

def find_container_name(output):
    return output.find('test_gitlab_container') != -1

def find_container_keys(output, keys):
    for key in keys:
        assert output.find(key) != -1, \
            'Error: could not find key: {}'.format(key)
