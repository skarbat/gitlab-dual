'''
test_postfix.py
'''

import runner

def test_mta_run(bash):
    s = runner.run_gitlab_cli(bash, ['mta-run'])
    assert s.find('network create') != -1
    assert s.find('--name postfix') != -1
    assert s.find('--net gitlab-net') != -1    
    assert s.find('ALLOWED_SENDER_DOMAINS=mydomain1.com mydomain2.com') != -1

def test_mta_stop(bash):
    s = runner.run_gitlab_cli(bash, ['mta-stop'])
    assert s.find('stop') != -1
    assert s.find('postfix') != -1

def test_mta_logs(bash):
    s = runner.run_gitlab_cli(bash, ['mta-logs'])
    assert s.find('logs') != -1
    assert s.find('postfix') != -1
