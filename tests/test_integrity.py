'''
test_integrity.py
'''

import runner

def test_integrity_hostname(bash):

    # FIXME: find a way to catch the script failure,
    # and find the error string from stdout
    failed = False
    try:
        s = runner.run_gitlab_cli(bash, ['integrity', 'nowhere.hostname.com'])
    except:
        failed = True

    assert failed == True
