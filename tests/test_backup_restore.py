'''
test_backup_restore.py
'''

import runner

def test_backup(bash):
    backup_keys = [ 'gitlab-backup create', 'gitlab-ctl backup-etc',
                    'Running post backup', 'post-backup test', 'SKIP=artifacts' ]

    s = runner.run_gitlab_cli(bash, ['backup'])
    assert runner.find_container_name(s) == True
    runner.find_container_keys(s, backup_keys)

def test_restore(bash):
    restore_keys = [ 'gitlab-ctl stop unicorn', 'gitlab-ctl stop puma',
                     'gitlab-ctl stop sidekiq', 'gitlab-ctl status',
                     'gitlab-backup restore --trace', 'tar cfzv', 'restart',
                     'Running post restore', 'post-restore test' ]

    s = runner.run_gitlab_cli(bash, ['restore'])
    assert runner.find_container_name(s) == True
    runner.find_container_keys(s, restore_keys)
