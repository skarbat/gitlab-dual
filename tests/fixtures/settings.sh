#!/bin/bash
#
#  settings.sh
#
#  This is a sample settings file.
#  It is sourced by gitlab-dual-cli during tests.
#

v_gitlab_version="gitlab_version_12345"
v_container_name="test_gitlab_container"
v_gitlab_homedir="tests"
v_gitlab_hostname="gitlab_hostname"
v_backup_filename="test"
v_backup_cfgfile="testcfg"
v_post_backup_script="tests/fixtures/post-backup.sh"
v_post_restore_script="tests/fixtures/post-restore.sh"

# MTA specific settings
v_docker_network_name="gitlab-net"
v_mta_allowed_domains="mydomain1.com mydomain2.com"
v_mta_docker_image="boky/postfix"
