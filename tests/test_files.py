'''
Make sure the expected scripts are in the bin folder.
'''

import os

script_files = [ 'bin/gitlab-dual-cli',
                 'etc/gitlab-dual-settings',
                 'share/gitlab-dual-functions' ]

def test_files():
    for script in script_files:
        assert os.path.isfile(script) == True, 'Script not found: {}'.format(script)
