'''
test_info.py
'''

import runner

def test_info(bash):
    s = runner.run_gitlab_cli(bash, ['info'])
    assert 'System' in s.split()
    assert 'Docker' in s.split()

def test_help(bash):
    s = runner.run_gitlab_cli(bash, ['-h'])
    assert s.find('version') != -1
