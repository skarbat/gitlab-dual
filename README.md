## Gitlab Dual

This repository provides a tool to deploy and maintain a dual Gitlab, Docker based, synchronized system.
Having two instances running allows for fault-tolerance and recovery in case of server failure.
But the tool can also be used on one single instance to automate backup, restore and upgrade operations.

For specific use case scenarios, see the [RECIPES.md](RECIPES.md) file.

### Requirements

You will need the following resources:

 * One or two computer systems running Debian or derivative linux.
 * Docker command line version up and running.
 * Admin access to the DNS records that point to the systems public IP addresses

### Building

The Makefile provides steps to run the tests and generate a Debian package:

```
$ make tests && make pkg

```

### Installation

Install the debian package with `sudo apt-get install ./gitlab-dual_1.0-1_all.deb`.

Edit the configuration file `/etc/gitlab-dual-settings` and set parameters to your environment.
Make sure parameters are correctly setup by running `gitlab-dual-cli info`.

Pick up a domain name and make sure the DNS records point to one of the server instances.
The domain should match the `v_gitlab_hostname` in the configuration file under /etc.

Make sure the directory referred to in the `v_gitlab_homedir` is available. This directory
will provide mappings to most relevant directories in the container (/etc, backups, etc).

### Running gitlab

To start a fresh new Gitlab instance, run:

```
$ gitlab-dual-cli run
```

After a while, you should be able to hit the hostname,
and be presented with the first Gitlab page to set your root password.

### Integrating a Mail agent

If you do not have a mail relay agent in place, `gitlab-dual` provides one for you based on Postfix.
Simply run the following:

```
$ gitlab-dual-cli mta-run
```

This command will start a Docker container running Postfix. Both Gitlab and Postfix
containers will be bound to the same Docker network - see the `v_docker_network_name` setting.

In order to connect Gitlab to the Postfix container for delivering emails,
you will need to apply at least the changes below in the `gitlab.rb` file,
followed by a `gitlab-dual-cli reconfigure` command:

```
gitlab_rails['smtp_enable'] = true
gitlab_rails['smtp_address'] = "postfix.gitlab-net"
gitlab_rails['smtp_port'] = 587
```

The `smtp_address` is the key setting: `postfix` is a hardcoded name that refers to the
Postfix container, while `gitlab-net` refers to the Docker bridged network name -
see the `v_docker_network_name` setting.

You can also use the `gitlab-dual-cli mta-stop` and `gitlab-dual-cli mta-logs` commands.

### Maintenance tasks

You have several commands for Gitlab container maintenance. Run `gitlab-dual help` for details.

If you need to apply changes to the `gitlab.rb`, you can do so like this:

 * Edit the `gitlab.rb` file under the `v_gitlab_homedir` directory
 * Run `gitlab-dual restart` command.

For additional gitlab internal commands you can enter an interactive shell with `gitlab-dual shell`.
You can follow the gitlab logs with `gitlab-dual logs`.

### Creating Backups

Simply run `gitlab-dual backup`. After a while the backups will be available under
the `v_gitlab_homedir/backups` folder. This will include a complete Gitlab data backup,
and the configuration under /etc, in 2 separate files.

At this point it is highly recommended to extract these 2 files outside the server so you can replicate
the same Gitlab instance on the second server.

If you need to perform additional steps during backup, add them to the script `post-backup.sh` referred to
from the configuration file.

### Restoring Backups

As explained above, the ideal setup is to have 2 servers and synchronize backups between them.
Therefore, move to the second server instance and copy the backup files from the step above under the
directory `v_gitlab_homedir/backups`, then run `gitlab-dual restore`.

On completion you should have a Gitlab replica up and running. Switching DNS records to the second
server should provide for a seamless switch.

If you need to perform additional steps during a restore, add them to the script `post-restore.sh` referred to
from the configuration file.

### Letsencrypt

Gitlab provides integration with letsencrypt, which is highly recommended in this setup.
The procedure involves some minor changes in the `gitlab.rb` file - see references section below.

### Upgrades

Gitlab upgrades when running inside Docker are amazingly easy to apply. Follow these steps:

 * Stop the currently running container: `gitlab-dual stop`
 * Increase the Gitlab version in the file `/etc/gitlab-dual-settings` - `v_gitlab_version` value
 * Start the Gitlab container once again: `gitlab-dual run`
   * Docker will download the new Gitlab image
   * Gitlab will reconfigure and update itself
   * It might take a little while before it starts serving web requests

*Caution*: Pay attention to the [upgrade version path](https://docs.gitlab.com/12.10/ee/policy/maintenance.html#upgrade-recommendations) as not all major version jumps are safe.

### Integrity checks

As part of the upgrade and backup/restore process, it becomes important to have a check process
that compares both Gitlab instances to make sure they are in sync.

The Python script `share/integrity-checks` provides this functionality. It connects via the Gitlab
Python API to your gitlab server, and collects a number of differents sets of information, it returns
a json file with all relevant details, as a data fingerprint. Usage:

```
$ gitlab-dual-cli integrity my.gitlab.hostname
```

Currently it collects data for:

 * Users, groups, repositories and registry
 * Summary section with information counters
 * License information

On return you should have the file `my.gitlab.hostname.json` on the current directory. You can easily use the
`jq` tool to peak at important values:

```
$ fq '.details' my.gitlab.hostname.json
{
  "hostname": "my.gitlab.hostname",
  "repo_registry_count": 11,
  "repository_count": 228,
  "user_count": 73
}
```

You will need to [define access details](https://python-gitlab.readthedocs.io/en/stable/cli.html#cli-configuration)
to connect to your gitlab instance.

### Gitlab Mail relay

As proposed from the official Gitlab documentation, it is highly recommended to have the mail relay
agent running on a separate docker container, and have Gitlab.rb settings point to it.

gitlab-dual provides two commands to integrate a Postfix docker image along with your Gitlab instance:

 * A docker network is created
 * Postfix is run and connected to the network
 * Gitlab is also run connected to the same network
 * The gitlab container reaches the postfix container through the `network.postfix` hostname.

Therefore, the two available commands are `gitlab-dual-cli mta-run` and `gitlab-dual-cli mta-stop`.
You can follow and debug the Postfix container with `gitlab-dual-cli mta-logs`.

See the configuration file for more details.

#### References

 * Running Gitlab in Docker: https://docs.gitlab.com/12.10/omnibus/docker/README.html
 * Gitlab backups: https://docs.gitlab.com/omnibus/settings/backups.html
 * Letsencrypt: https://docs.gitlab.com/omnibus/settings/ssl.html
 * Postfix Docker: https://github.com/bokysan/docker-postfix
 * Dockerhub postfix image: https://hub.docker.com/r/boky/postfix
 * Upgrade Gitlab in docker: https://copdips.com/2018/10/update-gitlab-in-docker.html
 * Python API: https://python-gitlab.readthedocs.io/en/stable/api-usage.html
 * SMTP settings: https://docs.gitlab.com/omnibus/settings/smtp.html
