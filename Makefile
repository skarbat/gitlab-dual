#
# Makefile
#
# Automate some tedious tasks
#

.PHONY: help tests pkg deps clean

all: help



help:
	@echo "Makefile for the Gitlab Dual software package"
	@echo "Available rules:"
	@echo " tests, pkg, deps, clean"

tests:
	shellcheck --shell=bash --color=always --check-sourced \
		--external-sources --exclude=SC1090 --exclude=SC2154 bin/*
	pytest -v tests

pkg:
	debuild -us -uc -b
	dpkg -c ../gitlab-dual*deb

deps:
	pip install -r requirements.txt
	sudo apt-get install shellcheck

clean:
	-find . -iname "*~" -exec rm -rf {} \;
	-find . -iname "*pyc" -exec rm -rf {} \;
	-find . -iname "__pycache__" -exec rm -rf {} \;
	-rm -rf .pytest_cache/ debian/.debhelper/ debian/gitlab-dual/
	-rm debian/files debian/*debhelper* debian/*substvars*
